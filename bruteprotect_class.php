<?php
/**
 * @file
 * Main class the bruteprotect module.
 */

/**
 * BruteProtect_class
 *
 * @package  BruteProtect_class
 * @license  http:// www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http:// bruteprotect.com
 */

class BruteProtect {

  /**
   * BP: brute_get_ip().
   */
  public function BruteGetClientIp() {
    if (isset($this->user_ip)) {
      return $this->user_ip;
    }

    /* Drupal's own ip_address() function does NOT cover all possible
    server variables that are known to exist. */
    $server_headers = array(
      'HTTP_CLIENT_IP',
      'HTTP_CF_CONNECTING_IP',
      'HTTP_X_FORWARDED_FOR',
      'HTTP_X_FORWARDED',
      'HTTP_X_CLUSTER_CLIENT_IP',
      'HTTP_FORWARDED_FOR',
      'HTTP_FORWARDED',
      'REMOTE_ADDR',
    );

    if (function_exists('filter_var')) {
      foreach ($server_headers as $key) {
        if (array_key_exists($key, $_SERVER) === TRUE) {
          foreach (explode(',', $_SERVER[$key]) as $ip) {
            $ip = trim($ip);
            if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== FALSE) {
              $this->user_ip = $ip;
              return $this->user_ip;
            }
          }
        }
      }
    }
    else {
      foreach ($server_headers as $key) {
        if (array_key_exists($key, $_SERVER) === TRUE) {
          foreach (explode(',', $_SERVER[$key]) as $ip) {
            $ip = trim($ip);
            $this->user_ip = $ip;
            return $this->user_ip;
          }
        }
      }
    }

  }

  /**
   * BP: isOnLocalhost().
   */
  public function isOnLocalhost() {
    $ip = $this->BruteGetClientIp();

    // Never block login from localhost.
    if ($ip == '127.0.0.1' || $ip == '::1') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * BP: BruteCheckLoginability().
   */
  public function BruteCheckLoginability($preauth = FALSE) {
    $ip = $this->BruteGetClientIp();

    // Never block login from localhost.
    if ($this->isOnLocalhost()) {
      return TRUE;
    }

    $transient_name = 'brute_loginable_' . str_replace('.', '_', $ip);
    $transient_value_cache = cache_get($transient_name);

    if (!empty($transient_value_cache->data)) {
      $transient_value = $transient_value_cache;
    }

    // Never block login from whitelisted IPs.
    $whitelist = variable_get('brute_ip_whitelist');
    $wl_items = explode(PHP_EOL, $whitelist);
    $iplong = ip2long($ip);

    if (is_array($wl_items)) {
      foreach ($wl_items as $item) {

        $item = trim($item);

        // Exact match.
        if ($ip == $item) {
          return TRUE;
        }

        // No match, no wildcard.
        if (strpos($item, '*') === FALSE) {
          continue;
        }
        $ip_low = ip2long(str_replace('*', '0', $item));
        $ip_high = ip2long(str_replace('*', '255', $item));

        // IP is within wildcard range.
        if ($iplong >= $ip_low && $iplong <= $ip_high) {
          return TRUE;
        }
      }
    }

    // Check out our transients.
    if (isset($transient_value) && $transient_value->data['status'] == 'ok') {
      return TRUE;
    }
    if (isset($transient_value) && $transient_value->data['status'] == 'blocked') {
      if ($transient_value->expire < time()) {
        /* The block is expired but the transient didn't go away naturally,
        clear it out and allow login. */
        cache_clear_all($transient_name, 'cache');
        return TRUE;
      }
      // There is a current block-- prevent login by returning FALSE.
      return FALSE;
    }

    // If we've reached this point, this means that the IP isn't cached.
    /* Now we check with the bruteprotect.com servers to see if we
    should allow login.
    */
    $response = $this->BruteCall('check_ip');

    if (isset($response['math']) && !function_exists('brute_math_authenticate')) {
      // The math feature/fields are in hook_form_alter() so return true here.
      return TRUE;
    }

    /* This IP is not cached but is blocked on the BP servers so we stop
    the login right here and add it to the cache. */
    if ($response['status'] == 'blocked') {
      $this->BruteLogBlockedAttempt();
      return FALSE;
    }

    return TRUE;
  }

  /**
   * BP: BruteLogFailedAttempt().
   */
  public function BruteLogFailedAttempt() {
    $this->BruteCall('failed_attempt');
  }

  /**
   * BP: BruteGetLocalHost().
   */
  public function BruteGetLocalHost() {
    if (isset($this->local_host)) {
      return $this->local_host;
    }

    $uri = 'http:// ' . strtolower($_SERVER['HTTP_HOST']);
    $uridata = parse_url($uri);
    $domain = $uridata['host'];

    // If we still don't have it, get the site_url.
    if (!$domain) {
      $uri = get_site_url(1);
      $uridata = parse_url($uri);
      $domain = $uridata['host'];
    }

    if (strpos($domain, 'www.') === 0) {
      $ct = 1;
      $domain = str_replace('www.', '', $domain, $ct);
    }

    $this->local_host = $domain;
    return $this->local_host;
  }

  /**
   * BP: GetBruteHost().
   */
  public function GetBruteHost() {
    if (isset($this->api_endpoint)) {
      return $this->api_endpoint;
    }
    $use_https = 'no';
    // Some servers can't access ssl, we'll check once a day to see if we can.
    $use_https_cache = cache_get('bruteprotect_use_https');
    if (!empty($use_https_cache->data)) {
      $use_https = $use_https_cache->data;
    }

    if (!$use_https) {
      $test = drupal_http_request('https://api.bruteprotect.com/https_check.php');
      $use_https = 'no';

      if ($test->data == 'ok') {
        $use_https = 'yes';
      }
      cache_set('bruteprotect_use_https', $use_https, 'cache', time() + 86400);
    }

    if ($use_https == 'yes') {
      $this->api_endpoint = 'https://api.bruteprotect.com/';
    }
    else {
      $this->api_endpoint = 'http://api.bruteprotect.com/';
    }

    return $this->api_endpoint;
  }

  /**
   * BP: BruteGetBlockedAttempts().
   */
  public function BruteGetBlockedAttempts() {
    return variable_get('bruteprotect_blocked_attempt_count');
  }

  /**
   * BP: BruteLogBlockedAttempt().
   */
  public function BruteLogBlockedAttempt($api_count = 0) {
    $attempt_count = $this->BruteGetBlockedAttempts();
    if ($attempt_count < $api_count) {
      $attempt_count = $api_count;
    }
    $attempt_count++;
    variable_set('bruteprotect_blocked_attempt_count', $attempt_count);
    return $attempt_count;
  }

  /*
  For some reason, drupal_http_request() does not work with the bruteprotect
  API service.  It always returns invalid key.  I believe it is because
  Drupal does not use Curl/PHP file_get_contents().  This fixes that issue
  but is irritating to have to do this.  We do not override
  drupal_http_request() because I do not have time to code an entire transport
  public function to replace it so that it doesn't break other modules.
  */

  /**
   * BP: CurlRequest().
   */
  public function CurlRequest($args, $type = 'GET') {
    $ch = curl_init();

    if ($type == 'POST') {
      /* If for some reason we need to modify the brute_url, we can override
      it here. IE: get_key requires index.php/get_key and not just index.php.*/
      if (!empty($args['brute_url'])) {
        $brute_host = $args['brute_url'];
        unset($args['brute_url']);
      }
      else {
        $brute_host = $this->GetBruteHost();
      }

      curl_setopt($ch, CURLOPT_URL, $brute_host);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
    }

    if ($type == 'GET') {
      // $args is not an array of args but instead is a full http/https url.
      curl_setopt($ch, CURLOPT_URL, $args);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    $results = curl_exec($ch);
    curl_close($ch);
    return $results;
  }

  /**
   * BP: phpHttpRequest().
   */
  public function phpHttpRequest($args, $type = 'GET') {
    if ($type == 'POST') {
      /* If for some reason we need to modify the brute_url, we can override it
      here.  IE: get_key requires index.php/get_key and not just index.php. */
      if (!empty($args['brute_url'])) {
        $brute_host = $args['brute_url'];
        unset($args['brute_url']);
      }
      else {
        $brute_host = $this->GetBruteHost();
      }
      $postdata = http_build_query($args);
      $opts = array(
        'http' => array(
          'method'  => 'POST',
          'header'  => 'Content-type: application/x-www-form-urlencoded',
          'content' => $postdata,
        ),
      );

      $context  = stream_context_create($opts);
      $result = file_get_contents($brute_host, FALSE, $context);

      return $result;
    }

    if ($type == 'GET') {
      // $args in this case is not an array of args, but a full http/https url.
      return file_get_contents($args);
    }
  }

  /**
   * BP: BruteRemoteRequest().
   */
  public function BruteRemoteRequest($args, $type = 'GET') {
    // Check for curl first.
    if (function_exists('curl_setopt')) {
      return $this->CurlRequest($args, $type);
    }

    // Check for file_get_contents() next.
    if (function_exists('file_get_contents') && ini_get('allow_url_fopen') == 1) {
      $orig_timout = ini_get('default_socket_timeout');
      ini_set('default_socket_timeout', 5);
      $results = @$this->phpHttpRequest($args, $type);
      ini_set('default_socket_timeout', $orig_timout);
      return $results;
    }

    /* If we are here, then no transport was found.  Tell the user to install
    Curl or enable remote file_get_contents(). */
    bruteprotect_set_message(t("You do not have php-curl installed nor is file_get_contents() allowed to access remote urls.  Please either install php-curl or enable http requests for file_get_contents() in the php.ini file with: <i>allow_url_fopen = 1</i> without one of these options, Bruteprotect in Drupal will not function."), 'error');
  }

  /**
   * BP: BruteCall().
   */
  public function BruteCall($action = 'check_ip', $options = array()) {
    if (!empty($options['api_key'])) {
      $api_key = $options['api_key'];
    }
    else {
      $api_key = variable_get('bruteprotect_api_key');
    }

    if (!empty($options['method'])) {
      $method = $options['method'];
      unset($options['method']);
    }
    else {
      $method = 'POST';
    }

    $drupal_version = VERSION;
    $brute_ua = "Drupal/{$drupal_version} | ";
    $brute_ua .= 'BruteProtect/' . constant('BRUTEPROTECT_VERSION');

    // Add options to the request array.
    if (count($options) > 0) {
      foreach ($options as $key => $value) {
        $request[$key] = $value;
      }
    }
    $request['action'] = $action;
    $request['ip'] = $this->BruteGetClientIp();
    $request['host'] = $this->BruteGetLocalHost();
    $request['blocked_attempts'] = $this->BruteGetBlockedAttempts();
    $request['privacy_settings'] = serialize(variable_get('brute_privacy_opt_in'));
    $request['bruteprotect_version'] = constant('BRUTEPROTECT_VERSION');
    $request['wordpress_version'] = $drupal_version;
    $request['drupal_version'] = $drupal_version;
    $request['api_key'] = $api_key;
    $request['multisite'] = 0;

    /*
    drupal_http_request() does not function properly so we have to replace it
    with another transport type (curl or file_get_contents())
    $post_args['method'] = 'POST';
    $post_args['data'] = http_build_query($args);
    $post_args['data'] = http_build_query($args, null, '&');
    $response_json = drupal_http_request($this->GetBruteHost(), $post_args);
    dpm($response_json);
    */
    $response_json = $this->BruteRemoteRequest($request, $method);

    /* Drupal's own ip_address() function does NOT cover all possible
    server variables that are known to exist. */
    $ip = $this->BruteGetClientIp();

    $transient_name = 'brute_loginable_' . str_replace('.', '_', $ip);
    cache_clear_all($transient_name, 'cache');

    if (!empty($response_json)) {
      $response = json_decode($response_json, TRUE);
    }
    /* If the response_json was a string and not actual json, we stop here and
    simply return it as the value.  IE: get_key does not return a json result. */
    if (json_last_error()) {
      return $response_json;
    }

    if (isset($response['status']) && !isset($response['error'])) {
      $response['expire'] = time() + $response['seconds_remaining'];
      cache_set($transient_name, $response, 'cache', time() + $response['seconds_remaining']);
      cache_clear_all('brute_use_math', 'cache');
    }
    else {
      // No response from the API host?  Let's use math captcha!
      cache_set('brute_use_math', 1, 'cache', time() + 600);
      $response['status'] = 'ok';
      $response['math'] = TRUE;
    }

    if (isset($response['error'])) :
      variable_set('bruteprotect_error', $response['error']);
    else :
      variable_del('bruteprotect_error');
    endif;

    return $response;
  }

  /**
   * BP: BruteDashboardWidget().
   */
  public function BruteDashboardWidget() {
    $key = variable_get('bruteprotect_api_key');
    $stats = $this->BruteRemoteRequest($this->GetBruteHost() . "index.php/dashboard/index/" . $key, 'GET');

    if (!empty($stats)) {
      return $stats;
    }

    return '<center><strong>Statistics are currently unavailable.</strong></center>';
  }
}
