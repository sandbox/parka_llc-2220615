How do I get an API Key?
Once the plugin is installed, you’ll be prompted to request an API Key via the
Admin area. If you do not receive an API Key, please contact us on drupal.org
to let us know.

Do I need a different key for each site?
Yes, each site needs a different key. Many API keys can be associated with one
email address.

Are there any known compatibility issues?
No! We've had no reported compatibility issues with any plugins.

Is it free?
Everything that we currently offer will always be free to individuals and small
businesses. We may, some day, ask for a (very small) fee from our highest 
activity users. We will offer additional “premium” services in spring 2014.

How many failed attempts before an IP address will be blocked?
This number varies based on a number of factors, there isn't a fixed number of
failed attempts that equal a block,  but it is set high enough to not
interfere with the request password functionality for a normal user.

Can I whitelist certain IP addresses?
Yes, you may add IP addresses under the “whitelist” setting of BruteProtect.
This list only applies to your installation of Drupal.
